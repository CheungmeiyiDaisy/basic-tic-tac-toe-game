import React from "react";
import Board, { BoardState } from "./Board";
import Square, { SquareProps } from "./Square";

export interface GameProps {
  //   history: string[];
  //   squares: string[];
  //   xIsNext: boolean;
}

export interface GameState {
  history: { squares: string[] }[];
  xIsNext: boolean;
  stepNumber: number;
}

export interface handleClick {
  history: { squares: string[] }[];
  squares: string[];
}

class Game extends React.Component<GameProps, GameState> {
  constructor(props: GameProps) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(""),
        },
      ],
      stepNumber: 0,
      xIsNext: true,
    };
  }

  //{'key':'value'}

  handleClick(i: number) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "❌" : "⭕";
    this.setState({
      history: history.concat([
        {
          squares: squares,
        },
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }
  jumpTo(step: any) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
    });
  }

  render() {
    const history: any = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step: any, move: any) => {
      const desc = move ? "Go to move #" + move : "Go to game start";
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (this.state.xIsNext ? "❌" : "⭕");
    }

    return (
      <div className="container">
        <div className="game">
          <div className="game-board">
            <Board
              squares={current.squares}
              onClick={(i: number) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
        <div className="Passage">
          <h1> Classic Tic-tac-toe game using React, Typescript,CSS </h1>
          <h2>Thank you for visiting </h2>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares: string[]) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export { Game, calculateWinner };
