import { type } from "os";
import React from "react";

export interface SquareProps {
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  value: string;
}

function Square(props: SquareProps) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

export default Square;
